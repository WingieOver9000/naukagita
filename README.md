![alt kursy](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/1280px-Git-logo.svg.png 'Logo gita')
# NaukaGita
**Opis:** Jest to repozytorium przeznaczone do nauki.
https://gitlab.com  
albo kliknij [tutaj](https://gitlab.com) abu przejsc do strony gitlab  

## Nagluwek 2

## Check lista:  
1. [x] Wprowadzenie  
    1. [x] Wstęp  
    2. [x] jak korzystać  
2. [x] Instalacja i konfig  
    1. [x] Wstęp 2  
    2. [x] jak korzystać 2 
3. [x] Instalacja i konfig  
    1. [x] Wstęp..  
    2. [x] jak korzystać..    
    3. [x] kolejny...
    4. [x] i nastepny
    5. [x] yeahh...
    6. [x] Marge request  
    7. [x] next comment
4. [x] branch development
    1. [x] kolejna linia dfevelop
    2. [x] 4.2 
    3. [x] 4.3  
    4. [x] 4.4
    5. [x] kolejna lekcjka
    6. [x] lekcja...
5. [x] force push
    1. [x] lekcja 5.1 
    2. [x] force push etc.
    3. [x] Prune  
    4. [x] usuwanie lokalnego repo
    5. [x] troche wiecej o git log
    6. [x] Tagging
6. [ ] Interactivbe staging
    1. [x] Interactivbe staging
    2. [ ] Patch mode
## Powiązane:

| Nazwa Kursu | Adres URL | Poziom kursu |
| ---------- | ---------- | ---------- |
| Dobre praktyki pracy| https://onet.pl | Średnio zaawansowany|
